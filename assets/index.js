import {
  start,
  oldPrompt,
  greeting,
  runCommandHelp,
  clear,
  runCommandAbout,
  runCommandSkills,
  runCommandSocials
} from "./scripts/utils.js"

const outputContainer = document.querySelector("#output")
const form = document.querySelector("#prompt-form")
const promptInput = document.querySelector("#prompt-input")

let user = "guest"

start(user, location.hostname)

form.addEventListener("submit", (event) => {
  sendCommand(event)
})

const sendCommand = (e) => {
  e.preventDefault()

  const oldCommand = promptInput.value
  promptInput.value = ""

  outputContainer.innerHTML += oldPrompt(oldCommand)

  const output = runCommand(oldCommand)
  outputContainer.innerHTML += `${output}`

  promptInput.focus()

  window.scrollTo(0, document.body.scrollHeight)
}

const runCommand = (command) => {
  const cmd = command.split(" ")[0]
  const allCmd = command.split(" ")

  let html = ""
  switch (cmd) {
    case "help":
      html = runCommandHelp()
      break
    case "welcome":
      html = greeting()
      break
    case "clear":
      clear()
      break
    case "about":
      html = runCommandAbout()
      break
    case "whoami":
      html = user
      break
    case "skills":
      html = runCommandSkills()
      break
    case "socials":
      html = runCommandSocials()
      break
    default:
      html = `command not found: ${cmd}`
  }

  return html
}
