export const commands = [
  {
    command: "about",
    subs: [],
    description: "about Sky Walker",
  },
  { command: "whoami", subs: [], description: "about current user" },
  { command: "welcome", subs: [], description: "print welcome greeting" },
  { command: "clear", subs: [], description: "clear the screen" },
  { command: "help", subs: [], description: "show available commands" },
  { command: "skills", subs: [], description: "print skills I've learned" },
  { command: "socials", subs: [], description: "print my social media links" },
]
