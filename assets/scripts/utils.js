import { commands } from "./commands.js"

export const start = (user, host) => {
  const username = document.querySelector(".prompt .info .username")
  const hostname = document.querySelector(".prompt .info .hostname")

  username.innerHTML = user
  hostname.innerHTML = host

  const outputContainer = document.querySelector("#output")

  outputContainer.innerHTML += `${oldPrompt("welcome")}`
  outputContainer.innerHTML += `${greeting()}`
}

export const oldPrompt = (commnad) => {
  const prompt = `
    <div class="prompt">
      <div class="info">
        <span class="username">guest</span>@<span class="hostname"
          >localhost</span
        >:~$
      </div>
      <input id="prompt-input" class="input" type="text" value="${commnad}"/>
    </div>
`

  return prompt
}

export const runCommandHelp = () => {
  let html = `<div>`
  commands.forEach((item) => {
    html += `<span class="command-item">${item.command}</span><span>${item.description}</span><br>`
  })
  html += "</div>"
  return html
}

export const runCommandAbout = () => {
  let html = "<div>"
  html += `Hi, my name is <strong>Bayu Prima Setiawan</strong>!
    <p class="mt-2">
      I am a <strong>full-stack developer</strong> based on East Java, Indonesia.<br>
      Mainly I use <strong>JavaScript</strong> for building web apps and mobile apps.
    </p>
  `
  html += "</div>"

  return html
}

export const runCommandSkills = () => {
  let langs = ["JS", "PHP", "SQL"]
  let frameworks = ["Vue", "React Native", "CodeIgniter"]

  let html = "<div>"
  html += `Here are technologies that I've learned
    <p class="mt-2">
      <strong>Programming Languages</strong>: ${langs.join(", ")}<br>
      <strong>Frameworks</strong>: ${frameworks.join(", ")}
    </p>
`
  html += "</div>"

  return html
}

export const runCommandSocials = () => {
  let socials = [
    {
      name: "Gitlab",
      link: "https://gitlab.com/meisskywalker/",
    },
    {
      name: "Linked In",
      link: "https://www.linkedin.com/in/bayu-prima-setiawan-38b837259/",
    },
  ]

  let html = "<div>"
  html += `<p class="mb-2">Here are my social media links</p>`
  socials.forEach((item, index) => {
    html += `<span class="command-item">${index + 1}. ${item.name}</span>
      <a class="link" href="${item.link}" target="_blank">${item.link}</a><br>`
  })
  html += "</div>"

  return html
}

export const clear = () => {
  const outputContainer = document.querySelector("#output")
  outputContainer.innerHTML = ""
}

export const greeting = () => {
  let version = "0.0.1"
  let welcome = `<pre>
   ______          _      __     ____          
  / __/ /____ __  | | /| / /__ _/ / /_____ ____
 _\\ \\/  \'_/ // /  | |/ |/ / _ \`/ /  '_/ -_) __/
/___/_/\\_\\\\_, /   |__/|__/\\_,_/_/_/\\_\\\\__/_/   
         /___/                version ${version}
</pre>`

  welcome += `<div>
    <br>
    Welcome to my portofolio.<br>
    For list of available commands, type \`<span class="command">help</span>\`
    </div>`
  return welcome
}
